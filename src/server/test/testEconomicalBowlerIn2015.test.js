const iplData = require(`${__dirname}/../index`)
const matches = iplData.matchConvertedToJSON;
const matchesWon = require('../ipl.js')
const deliveries = iplData.delivery;

let matchSize =Object.keys(matches).length;
let deliveriesSize =Object.keys(deliveries).length;

try {
    test(`matches length should not to be object`, ()=>{
        expect(typeof matches).toBe('object');
    })
    test(`matches length should not to be zero`, ()=>{
        let expected =0;
        expect(matchSize).not.toBe(expected);
    })
    for (let key in matches){
        test(`searching for ${key}season`,()=>{
            expect(typeof matches[key].season).toEqual('string');
            expect(key).not.toEqual('undefined');
            
        })
    }
    
    test(`match should be object`, ()=>{
        expect( matches)
    })
    test(`deliveries length should not to be zero`, ()=>{
        let expected =0;
        expect(deliveriesSize).not.toBe(expected);
    })
    for (let key in deliveries){
        test(`extra_run for ${key} should not be undefined`, ()=>{
            expect(typeof deliveries[key].extra_runs).not.toBe(undefined)
        })
    }
    for (let key in deliveries){
        test(`ball for ${key} should not be undefined`, ()=>{
            expect(typeof deliveries[key].ball).not.toBe(undefined)
        })
    }
    for (let key in deliveries){
        test(`batsman_runs for ${key} should not be undefined`, ()=>{
            expect(typeof deliveries[key].batsman_runs).not.toBe(undefined)
        })
    }
} catch (error) {
    console.log(error);
}



try {
    let  topEconomicalBowler = matchesWon.topEconomicalBowlerIn2015(matches,deliveries);
    module.exports = {topEconomicalBowler};
    console.log(topEconomicalBowler);
  
} catch (error) {
    console.log(error.message)
}
