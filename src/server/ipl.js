// -------------------------------matches won per year--------------------------------------------
const fs = require('fs')
function matchesWonPerYear(matches){
    let matchesPerSeasons = {};
   
    Object.keys(matches).forEach( (key) => {
        if (matches[key].season in matchesPerSeasons){
            matchesPerSeasons[matches[key].season] += 1
        }else{
            matchesPerSeasons[matches[key].season] = 1
        }
    })
    return matchesPerSeasons;
}

// -------------------------------matchesWonPerTeamPerYear-----------------------
// const fs = require('fs')
function matchesWonPerTeamPerYear(matches){
    let matchesWonPerSeason = {};
  
    let allSeasons = Object.keys(matches).map((key) =>  (matches[key].season));       
    let seasons = allSeasons.filter((item, index,ar) => ar.indexOf(item) === index);        
    let allWonTeam = Object.keys(matches).map((key) => matches[key].team1 );           
    allWonTeam = Object.keys(matches).map((key) => matches[key].team2 );               
    let wonTeam = allWonTeam.filter((item,index,arr) => arr.indexOf(item) === index);         
    Object.keys(seasons).forEach((yearId) => {                                         
        let temp = {};
        Object.keys(wonTeam).forEach((teamId) => temp[wonTeam[teamId]]=0);           
        matchesWonPerSeason[seasons[yearId]] = temp;
    })
    Object.keys(matches).forEach((matchId) => {                                       // Here i am checking with if condition that if winner of matches is not undefined then i have to increase the count by 1 of object matchesWonPerSeason
        if (typeof matches[matchId].winner != 'undefined'){
            matchesWonPerSeason[matches[matchId].season][matches[matchId].winner] += 1;
        }
    })
    return matchesWonPerSeason;

}

//-------------------------extraRunPerTeamIn2016-----------------------------------------------
// const fs = require('fs')

function extraRunPerTeamIn2016(matches,deliveries){
   
    let extraRunPerTeam={};
    let matchID = Object.keys(matches).filter((id) => matches[id].season === '2016');     
    let allWonTeam = Object.keys(matches).map((key) =>  (matches[key].team1))  
    allWonTeam = Object.keys(matches).map((key) =>   (matches[key].team2))     
    let wonTeam = allWonTeam.filter((item,i,arr) => arr.indexOf(item) === i);  // Extracting unique team from allTeam in wonTeam
    wonTeam.forEach((teamName) => (extraRunPerTeam[teamName] = 0) )        
    matchID.forEach((key) =>  extraRunPerTeam[deliveries[key]['batting_team']] += parseInt(deliveries[key]['extra_runs']))    // Here,in season 2016, with respect to matchID we are adding extra_runs of deliveries in extraRunPerTeam
    return extraRunPerTeam;
    
}
module.exports = {};

// --------------------------------- topEconomicalBowlerIn2015----------------------------------
function topEconomicalBowlerIn2015(matches,deliveries){
  
    let bowlerRun = {}
    let bowlerBall = {};
    let economicalRate = {};
    let topTenBowler = {};
    let matchID = Object.keys(matches).filter((id) => (matches[id].season === '2015')) 
    matchID.forEach((id) => {                                     
        bowlerBall[deliveries[id].bowler] = 0;
        bowlerRun[deliveries[id].bowler] = 0;
    })
    matchID.forEach( (id) => (bowlerRun[deliveries[id].bowler] += parseInt(deliveries[id].batsman_runs) ) )   
    matchID.forEach(function(key){                                 
        if (deliveries[key].wide_runs == 0 && deliveries[key].noball_runs == 0) {
            bowlerBall[deliveries[key].bowler] += parseInt(deliveries[key].ball);
        }
    })
    Object.keys(bowlerBall).forEach((bowlerName) => {
        economicalRate[bowlerName] = ((bowlerRun[bowlerName]/bowlerBall[bowlerName])*6) })   // Using formula for calculating Economical rate of every bowler
    let unsorted = Object.entries(economicalRate);                 
    let sorted = unsorted.sort((a, b) => a[1] -b[1]);              
    let tenBowler = sorted.slice(0,10);                            
    tenBowler.forEach((key) => (topTenBowler[key[0]] = key[1]) );  
    return topTenBowler;
    
}
module.exports = {matchesWonPerYear,matchesWonPerTeamPerYear,extraRunPerTeamIn2016,topEconomicalBowlerIn2015};